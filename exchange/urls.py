

from django.urls import path
from django.urls.conf import include
from . import views
from .views import ListTodo, DetailTodo, StudentView, UserViewSet, exchangeAPIView
from rest_framework import routers


router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='api_auth')),
    path('student/', StudentView.as_view()),

    path('exchange/<str:country_code>/<str:currency_code>/', exchangeAPIView.as_view()),
    path('<int:pk>/', DetailTodo.as_view()),
    path('listtodo/', ListTodo.as_view()),
]
