from django.contrib import admin
from .models import Todo, Student

admin.site.register(Todo)
admin.site.register(Student)
