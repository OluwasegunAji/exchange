from django.shortcuts import render

import requests
from rest_framework import generics
from exchange.models import Todo, Student
from exchange.serializers import TodoSerializer, UserSerializer, StudentSerializer
from django.contrib.auth.models import User
from rest_framework import viewsets
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class exchangeAPIView(generics.GenericAPIView):
  def get(self,request, country_code,currency_code):
   
      url = f'http://52.56.89.198:1881/api/mobile/money/rate/inquiry/{country_code}/USD/{currency_code}'
      url1 = f'http://52.56.89.198:1881/api/mobile/money/rate/inquiry/NG/USD/NGN'
      try:
         response = requests.get(url)
         nigeria_rate = requests.get(url1)
         if not response.status_code == 200:
               return("fail to create get exchange rate")
         else:     
               response = response.json()
               exchange_rate = response.get('fxRate')
               nigeria_rate = nigeria_rate.json()
               n_exchange_rate = nigeria_rate.get('fxRate')
               exch = (exchange_rate/n_exchange_rate )       
         return Response (exch)     
      except Exception as e:
         return Response(e) 
         

class ListTodo(generics.ListAPIView):
   queryset = Todo.objects.all()
   serializer_class = TodoSerializer


class DetailTodo(generics.RetrieveAPIView):
   queryset = Todo.objects.all()
   serializer_class = TodoSerializer

class StudentView(APIView):

   #permission_classes= (IsAuthenticated)

   serializer = StudentSerializer
   def get_queryset(self):
      res = Student.objects.all()
      return res

   def get(self, request, *args, **kwargs):
      qs = self.get_queryset()
      serializer = StudentSerializer(qs, many=True)
      return Response(serializer.data)

   def post(self, request, *args, **kwargs):
      serializer = StudentSerializer(data=request.data)
      if serializer.is_valid():
         serializer.save()
         return Response(serializer.data)
      return Response(serializer.errors)   


 