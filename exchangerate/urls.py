"""exchangerate URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls.conf import include
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

import exchangerate
from django.contrib import admin
from django.urls import path
  
schema_view = get_schema_view(
   openapi.Info(
      title="EXCHANGE API",
      default_version='v1',
      description="exchange rate",
      terms_of_service="https://www.google.com/policies/terms/",
      contact=openapi.Contact(email="oluwasegunajisafe@yahoo.com"),
      license=openapi.License(name="SEGUN@Aji"),
   ),
   public=True,
   permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    path('admin/', admin.site.urls),
   

    path('', include('exchange.urls')),
    
    path('api-auth/', include('rest_framework.urls')),
    path("doc", schema_view.without_ui(cache_timeout=0), name='schema-json'),
    path("doc1", schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path("redoc/", schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
   
]

# admin.site.site_header = "FROM SEGUN"
# admin.site.site_title = "GOOD"
# admin.site.index_title = "WELCOME"